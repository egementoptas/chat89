//
//  UserItem.swift
//  Chat89
//
//  Created by egemen toptaş on 27/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

class UserItem: BaseItem {
    var avatarUrl = ""
    var id = 0
    var nickname = ""
}
