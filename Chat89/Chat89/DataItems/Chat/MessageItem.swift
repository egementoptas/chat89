//
//  MessageItem.swift
//  Chat89
//
//  Created by egemen toptaş on 27/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

class MessageItem: BaseItem {
    var id = 0
    var text = ""
    var timestamp: Double = 0
    var user = UserItem()
}
