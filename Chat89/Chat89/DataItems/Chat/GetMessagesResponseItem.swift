//
//  GetMessagesResponseItem.swift
//  Chat89
//
//  Created by egemen toptaş on 27/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

class GetMessagesResponseItem: BaseItem {
    var messages = [MessageItem]()
}
