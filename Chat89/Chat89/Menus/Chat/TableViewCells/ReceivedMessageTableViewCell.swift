//
//  ReceivedMessageTableViewCell.swift
//  Chat89
//
//  Created by egemen toptaş on 27/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit
import Kingfisher

class ReceivedMessageTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var imageUserAvatar: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelMessageDate: UILabel!
    @IBOutlet weak var constraintUsernameHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    var defaultUsernameHeight: CGFloat = 17
        
    //MARK: - View Lifecyle
    override func awakeFromNib() {
        super.awakeFromNib()
        defaultUsernameHeight = constraintUsernameHeight.constant
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Setup
    func setup(with messageItem: MessageItem, isContinuedMessage: Bool) {
        setupViews(for: isContinuedMessage)
        setupAvatar(with: messageItem.user.avatarUrl)
        setupLabels(with: messageItem)
    }
    
    fileprivate func setupViews(for isContinuedMessage: Bool) {
        labelUsername.isHidden = isContinuedMessage
        imageUserAvatar.isHidden = isContinuedMessage
        constraintUsernameHeight.constant = isContinuedMessage ? 0 : defaultUsernameHeight
    }
    
    fileprivate func setupAvatar(with avatatUrl: String) {
        if let avatarUrl = URL(string: avatatUrl) {
            imageUserAvatar.kf.setImage(with: avatarUrl, placeholder: nil, options: [.transition(.fade(1))])
        } else {
            imageUserAvatar.image = nil
        }
    }
    
    fileprivate func setupLabels(with messageItem: MessageItem) {
        labelMessage.text = messageItem.text
        labelUsername.text = messageItem.user.nickname
        
        let date = Date(timeIntervalSince1970: messageItem.timestamp)
        let dateString = DateFormatter.localizedString(from: date, dateStyle: .short, timeStyle: .short)
        labelMessageDate.text = dateString
    }
}
