//
//  ChatViewController.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit
import Alamofire

class ChatViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableViewChat: UITableView!
    @IBOutlet weak var constraintTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTextViewContainerToBottom: NSLayoutConstraint!
    @IBOutlet weak var viewTextViewContainer: UIView!
    @IBOutlet weak var textViewMessageInput: UITextView!
    @IBOutlet weak var buttonSendComment: UIButton!
    
    //MARK: - Variables
    let notificationCenter = NotificationCenter.default
    let maxTextViewLines: CGFloat = 6
    var minTextViewHeight: CGFloat = 0
    var messages: [MessageItem] = []

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        addNotificationObservers()
        getPreviousMessages()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    //MARK: - Setup
    func setup() {
        navigationItem.title = ActiveUserHelper.sharedInstance.nickname
        minTextViewHeight = constraintTextViewHeight.constant
        buttonSendComment.setTitle(NSLocalizedString("SEND", comment: ""), for: .normal)
    }
    
    //MARK: - Keyboard Notifications and Handlers
    func addNotificationObservers() {
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShowHandler), name: .UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHideHandler), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShowHandler(_ notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let animationTime = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double else {
                return
        }
        constraintTextViewContainerToBottom.constant = keyboardSize.height
        tableViewChat.contentInset.bottom = keyboardSize.height + viewTextViewContainer.bounds.height
        UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            let lastIndexPath = IndexPath(row: self.messages.count - 1, section: 0)
            self.tableViewChat.scrollToRow(at: lastIndexPath, at: .none, animated: true)
        })
    }
    
    func keyboardWillHideHandler(_ notification: Notification) {
        guard let animationTime = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double else {
            return
        }
        constraintTextViewContainerToBottom.constant = 0
        tableViewChat.contentInset.bottom = viewTextViewContainer.bounds.height
        UIView.animate(withDuration: animationTime, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK: - Button Actions
    @IBAction func clickedToSendButton(_ sender: Any) {
        checkCurrentMessage()
    }
    
    //MARK: - Message Check
    func checkCurrentMessage() {
        guard let currentMessage = textViewMessageInput.text else {
            return
        }
        
        if currentMessage.trimWhiteSpaceAndNewLines() != "" {
            send(message: currentMessage.trimWhiteSpaceAndNewLines())
        }
    }
    
    func send(message: String) {
        let messageItem = MessageItem()
        messageItem.text = message
        messageItem.timestamp = Date().timeIntervalSince1970
        messageItem.user.nickname = ActiveUserHelper.sharedInstance.nickname
        
        messages.append(messageItem)
        let indexPath = IndexPath(row: messages.count - 1, section: 0)
        tableViewChat.beginUpdates()
        tableViewChat.insertRows(at: [indexPath], with: .none)
        tableViewChat.endUpdates()
        textViewMessageInput.text = ""
        constraintTextViewHeight.constant = minTextViewHeight
        textViewMessageInput.isScrollEnabled = false
        tableViewChat.scrollToRow(at: indexPath, at: .none, animated: true)
    }
}

//MARK: - UITextView Delegate
extension ChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let textViewVerticalInset = textViewMessageInput.textContainerInset.bottom + textViewMessageInput.textContainerInset.top
        let maxHeight = ((textViewMessageInput.font?.lineHeight ?? 15.0) * maxTextViewLines) + textViewVerticalInset
        let sizeThatFits = textViewMessageInput.sizeThatFits(CGSize(width: textViewMessageInput.frame.size.width, height: CGFloat.greatestFiniteMagnitude))

        if sizeThatFits.height < minTextViewHeight {
            constraintTextViewHeight.constant = minTextViewHeight
            textViewMessageInput.isScrollEnabled = false
        } else if sizeThatFits.height < maxHeight {
            constraintTextViewHeight.constant = sizeThatFits.height
            textViewMessageInput.isScrollEnabled = false
        } else {
            constraintTextViewHeight.constant = maxHeight
            textViewMessageInput.isScrollEnabled = true
        }
    }
}

//MARK: - UITableView Data Source
extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        //Checking if message is sent by the same user with previous one, to hide avatar and username on continued messages.
        let isContinuedMessage = indexPath.row > 0 && message.user.nickname == messages[indexPath.row - 1].user.nickname
        
        //Checking with nickname because active user does't have UserId.
        if message.user.nickname == ActiveUserHelper.sharedInstance.nickname {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SentMessageTableViewCell", for: indexPath) as! SentMessageTableViewCell
            cell.setup(with: message, isContinuedMessage: isContinuedMessage)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceivedMessageTableViewCell", for: indexPath) as! ReceivedMessageTableViewCell
            cell.setup(with: message, isContinuedMessage: isContinuedMessage)

            return cell
        }
    }
}

//MARK: - UITableView Delegate
extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10 //FIXME: magic O_o
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        textViewMessageInput.endEditing(true)
    }
}

//MARK: - Network
extension ChatViewController {
    func getPreviousMessages() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(API.previousMessages, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let item = GetMessagesResponseItem(dictionary: value as! NSDictionary)
                    //Since there is no mention of message sorting in documents assuming sorting is done on client side.
                    self.messages = item.messages.sorted(by: { $0.timestamp < $1.timestamp})
                    self.tableViewChat.reloadData()
                case .failure(let error):
                    self.showAlert(for: error)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    //MARK: - Error Handling
    func showAlert(for error: Error) {
        let ac = ErrorHelper.getUIAlert(for: error)
        let retryAction = UIAlertAction(title: NSLocalizedString("TRY_AGAIN", comment: ""), style: .default, handler: { _ in
            self.getPreviousMessages()
        })
        ac.addAction(retryAction)
        self.present(ac, animated: true, completion: nil)
    }
}
