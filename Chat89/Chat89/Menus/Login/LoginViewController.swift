//
//  LoginViewController.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var textfieldNickname: UITextField!
    @IBOutlet weak var labelWarning: UILabel!
    @IBOutlet weak var buttonContinue: UIButton!
    
    //MARK: - Variables
    let minimumCharacterCountForUsername = 3
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLoginState()
        setup()
    }
    
    //MARK: - Login Check
    func checkLoginState() {
        //Could also change initial Storyboard from AppDelegate but this workaround felt better for a simple two page app.
        if ActiveUserHelper.sharedInstance.nickname != "" {
            navigationController?.pushViewController(ViewControllers.chat, animated: false)
        }
    }
    
    //MARK: - Setup
    func setup() {
        self.view.addDefaultGradientLayerToBackground()
        navigationItem.title = "Chat89"
        navigationItem.backBarButtonItem?.title = NSLocalizedString("LEAVE", comment: "")
        labelWarning.alpha = 0
        textfieldNickname.text = ActiveUserHelper.sharedInstance.nickname
    }
    
    //MARK: - Button Actions
    @IBAction func clickedToContinueButton(_ sender: Any) {
        checkEnteredNickname()
    }
    
    //MARK: - Nickname Check
    func checkEnteredNickname() {
        if isNicknameValid() {
            //It's safe to force unwrap text here, because we already checked if it is valid.
            ActiveUserHelper.sharedInstance.save(nickname: textfieldNickname.text!.trimWhiteSpaceAndNewLines())
            textfieldNickname.endEditing(true)
            goToChat()
            hideWarningForInvalidNickname()
        } else {
            showWarningForInvalidNickname()
        }
    }
    
    func isNicknameValid() -> Bool {
        guard let enteredNickname = textfieldNickname.text?.trimWhiteSpaceAndNewLines() else {
            return false
        }
        
        return enteredNickname.characters.count >= minimumCharacterCountForUsername
    }
    
    func showWarningForInvalidNickname() {
        labelWarning.text = NSLocalizedString("INVALID_NICKNAME_WARNING", comment: "")
        UIView.animate(withDuration: 0.5, animations: {
            self.labelWarning.alpha = 1
        })
        
        textfieldNickname.shake()
    }
    
    func hideWarningForInvalidNickname() {
        UIView.animate(withDuration: 0.3, animations: {
            self.labelWarning.alpha = 0
        })
    }
    
    //MARK: - Navigation
    func goToChat() {
        navigationController?.pushViewController(ViewControllers.chat, animated: true)
    }
}

//MARK: - UITextField Delegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        checkEnteredNickname()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        textField.layoutIfNeeded()
    }
}
