//
//  ActiveUserHelper.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

class ActiveUserHelper {
    
    fileprivate static var _sharedInstance: ActiveUserHelper?
    static var sharedInstance: ActiveUserHelper {
        if _sharedInstance == nil {
            _sharedInstance = ActiveUserHelper()
        }
        return _sharedInstance!
    }
    
    let defaults = UserDefaults.standard
    
    var nickname = ""
    
    func save(nickname: String) {
        self.nickname = nickname
        let archivedNickname: Data = NSKeyedArchiver.archivedData(withRootObject: nickname)
        defaults.set(archivedNickname, forKey: "nickname")
        defaults.synchronize()
    }
    
    func loadSavedUserData() {
        guard let loadedNicknameData = defaults.object(forKey: "nickname") as? Data else { return }
        guard let loadedNickname = NSKeyedUnarchiver.unarchiveObject(with: loadedNicknameData) as? String else { return }

        nickname = loadedNickname
    }
}
