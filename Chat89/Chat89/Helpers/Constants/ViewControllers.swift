//
//  ViewControllers.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

struct ViewControllers {
    static var login: LoginViewController {
        return Storyboards.login.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }
    static var chat: ChatViewController {
        return Storyboards.chat.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
    }
}
