//
//  Colors.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

struct Colors {
    struct BackgroundGradient {
        static let light        = UIColor(red: 0.00000, green: 0.40784, blue: 0.52157, alpha: 1.00000)
        static let dark         = UIColor(red: 0.43922, green: 0.70588, blue: 0.72157, alpha: 1.00000)
    }
    
    static var standartButtonColor = UIColor(red: 0/255, green: 150/255, blue: 136/255, alpha: 1.0)
}
