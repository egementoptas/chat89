//
//  Storyboards.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

struct Storyboards {
    static var launchScreen: UIStoryboard {
        return UIStoryboard(name: "LaunchScreen", bundle: nil)
    }
    static var login: UIStoryboard {
        return UIStoryboard(name: "Login", bundle: nil)
    }
    static var chat: UIStoryboard {
        return UIStoryboard(name: "Chat", bundle: nil)
    }
}
