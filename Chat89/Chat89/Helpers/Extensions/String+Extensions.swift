//
//  String+Extensions.swift
//  Chat89
//
//  Created by egemen toptaş on 26/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

extension String {
    func trimWhiteSpaces() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func trimWhiteSpaceAndNewLines() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
