//
//  ErrorHelper.swift
//  Chat89
//
//  Created by egemen toptaş on 27/07/2017.
//  Copyright © 2017 egemen. All rights reserved.
//

import UIKit

class ErrorHelper {
    
    static func getUIAlert(for error: Error) -> UIAlertController {
        var title = ""
        var message = ""
        switch (error as NSError).code {
        case -1001:
            title = NSLocalizedString("ERROR", comment: "")
            message = NSLocalizedString("TIME_OUT", comment: "")
        case -1009:
            title = NSLocalizedString("NO_CONNECTION_TITLE", comment: "")
            message = NSLocalizedString("NO_CONNECTION_MESSAGE", comment: "")
        case -1004:
            title = NSLocalizedString("ERROR", comment: "")
            message = NSLocalizedString("CANT_REACH_TO_SERVERS", comment: "")
        default:
            title = NSLocalizedString("ERROR", comment: "")
            message = NSLocalizedString("UNKNOWN_ERROR", comment: "")
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
}
